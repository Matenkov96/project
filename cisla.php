<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
  Сумма простых чисел, меньших 10 - это 2 + 3 + 5 + 7 = 17.
Напишите программу на PHP, которая найдет сумму всех простых чисел, меньших двух миллионов:
  <?php
  $i=1;
  $stack = array();
  while ($i <= 2000000) {
	 $prost = gmp_prob_prime ($i);
	 if($prost==2){
	  array_push($stack, $i);
	 }
	  $i=$i+1;
  }
  echo array_sum($stack);
  ?>
  </body>
</html>
